<?php
require_once('./bibli_gazette.php');
require_once('./bibli_generale.php');

// bufferisation des sorties
ob_start();

// démarrage de la session
session_start();

// affichage de l'entête
hm_aff_entete('Rédaction', 'Rédaction');
hml_aff_redac();
hm_aff_pied();
ob_end_flush();

/**
 * Affiche les redacteurs triés par leur catégories :
 * Redacteur en Chef, Premiers violons, Sous-fifres
 */
function hml_aff_redac(){
    echo '<main>',
            '<section>',
                '<h2>Le mot de la rédaction</h2>',
                '<p>Passionnés par le journalisme d\'investigation depuis notre plus jeune âge, nous avons créé en 2019 ce site pour répondre à un', 
                'réel besoin : celui de fournir une information fiable et précise sur la vie de la <abbr title="Licence Informatique">L-INFO </abbr>',
                'de l\'<a href="http://www.univ-fcomte.fr" target="_blank">Université de Franche-Comté</a>.</p>',
                '<p>Découvrez les hommes et les femmes qui composent l\'équipe de choc de la Gazette de L-INFO. </p>',
            '</section>';

    $bd = hm_bd_connecter();
    //Requete qui recupere les infos d'utilisateurs de tout les rédacteurs.
    $sql="SELECT reBio,reCategorie,reFonction,utPseudo,utNom,utPrenom,utStatut FROM `redacteur`,utilisateur WHERE rePseudo=utPseudo";
    //Requete qui recupere le nombre de personne d'une categorie et qui a encore le statut redacteur
    $sql2="SELECT 
    (SELECT COUNT(reCategorie) FROM redacteur,utilisateur WHERE rePseudo=utPseudo AND (utStatut=1 OR utStatut=3) AND reCategorie=1) AS CAT1, 
    (SELECT COUNT(reCategorie) FROM redacteur,utilisateur WHERE rePseudo=utPseudo AND (utStatut=1 OR utStatut=3) AND reCategorie=2) AS CAT2, 
    (SELECT COUNT(reCategorie) FROM redacteur,utilisateur WHERE rePseudo=utPseudo AND (utStatut=1 OR utStatut=3) AND reCategorie=3) AS CAT3";
    $res2=mysqli_query($bd, $sql2) or hm_bd_erreur($bd, $sql2);
    $tab2=mysqli_fetch_row($res2);
    $nomSectionPresent=false;
    //Affiche les 3 sections (categories)
    for($i=1;$i<=3;$i++){
        $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
        hml_aff_section_redac($res,$tab2,$nomSectionPresent,$i);
        mysqli_free_result($res);
    }
    mysqli_free_result($res2);
    echo '<section>',
        '<h2>La Gazette de L-INFO recrute !</h2>',
        '<p>Si vous souhaitez vous aussi faire partie de notre team, rien de plus simple. Envoyez-nous un mail grâce au lien dans le menu de navigation, et rejoignez l\'équipe. </p>',
        '</section>',
        '</main>';
}

function hml_aff_section_redac($res,$tab2,$nomSectionPresent,$num_cat){
    //Parcours du tableau des redacteurs
    while($tab=mysqli_fetch_assoc($res)){
        $tab=hm_html_proteger_sortie($tab);
        //Affiche les rédacteurs de catégorie $num_cat avec une bio présente et avec le statut rédacteur
        if($tab['reCategorie']==$num_cat && ($tab['reBio']!='') && (($tab['utStatut']==1)||($tab['utStatut']==3))){
            //Si faux alors on affiche une nouvelle section
            if(!$nomSectionPresent){
                echo '<section>',
                '<h2>';
                //Si 1 seule personne dans la categorie alors Notre sinon Nos (affiche titre section)
                if($tab2[$num_cat-1]<=1){
                    //Titre selon numero de categorie
                    if ($num_cat == 1) {
                        echo 'Notre rédacteur en chef';
                    } elseif ($num_cat == 2) {
                        echo 'Notre premier violon';
                    } elseif ($num_cat == 3) {
                        echo 'Notre sous-fifre';
                    } 
                }else{
                    //Titre selon numero de categorie
                    if ($num_cat == 1) {
                        echo 'Nos rédacteurs en chef';
                    } elseif ($num_cat == 2) {
                        echo 'Nos premiers violons';
                    } elseif ($num_cat == 3) {
                        echo 'Nos sous-fifres';
                    } 
                }
                
                echo '</h2>';
                $nomSectionPresent=true;  
            }
            echo '<article class="redacteur" id="',$tab['utPseudo'],'">';
            if(is_file("../upload/{$tab['utPseudo']}.jpg")){
                $imgFile = "../upload/{$tab['utPseudo']}";
            }else{
                $imgFile = "../images/anonyme";
            }
            echo '<img src="',$imgFile,'.jpg" width="150" height="200" alt="',$tab['utPrenom'],' ',$tab['utNom'],'">',
            '<h3>',$tab['utPrenom'],' ',$tab['utNom'],'</h3>',
            hm_BBCodeToHTML($tab['reBio']),
            '</article>';
        }
    }
    //Si nom de section est present alors on ferme la section.
    if($nomSectionPresent){
        echo '</section>';
        $nomSectionPresent=false;
    }
}