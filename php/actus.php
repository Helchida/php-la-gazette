<?php
require_once('./bibli_gazette.php');
require_once('./bibli_generale.php');

// bufferisation des sorties
ob_start();

// démarrage de la session
session_start();

// affichage de l'entête
hm_aff_entete('L\'actu', 'L\'actu');
hml_aff_actus();
hm_aff_pied();

ob_end_flush();

/**
 * Affiche les articles (résumé,image,titre) avec un maximum de 4 article par pages
 * Présence du choix de page (boutons)
 *  
 */
function hml_aff_actus(){
    $bd = hm_bd_connecter();
    //Requete qui compte le nombre d'article
    $sql="SELECT COUNT(*) as NB_ARTICLE FROM article ";
    $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    $tab=mysqli_fetch_assoc($res);
    $tab=hm_html_proteger_sortie($tab);
    mysqli_free_result($res);
    $nb_article=$tab['NB_ARTICLE'];
    //Calcule le nombre de page pour affiché 4 articles par pages
    $nb_pages=ceil($nb_article/4);

    echo '<main>',
            '<section>',
                '<form action="actus.php" method="post">',
                    '<tr>',
                        '<td>',
                            'Pages :';
                            //Affichage des boutons de pages.
                            for($i=1;$i<=$nb_pages;$i++){
                                echo '<input type="submit" name="btnPage',$i,'" value="',$i,'">';
                            }
                    echo '</td>',
                    '</tr>',
                '</form>';              
            echo '</section>';
            
            $valueBouton=1;
            //Parcours des boutons pour savoir lequel est sélectionné
            for($j=1;$j<=$nb_pages;$j++){
                $nameCurrent="btnPage".$j;
                if(isset($_POST[$nameCurrent])){
                    $valueBouton=$j;
                    break;
                }
            }
            $debutArticle=($valueBouton-1)*4;
            //Requete qui selectionne les articles par ordre décroissant de publication et avec une limite de 4 articles 
            $sql="SELECT arID, arTitre, arResume, arDatePublication FROM article ORDER BY arDatePublication DESC LIMIT 4 OFFSET {$debutArticle} ";
            $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
            $mois_identique=false;
            $moisVerif='';
            $anneeVerif=0;
            //Parcours des articles
            while($tab2=mysqli_fetch_assoc($res)){
                $tab2=hm_html_proteger_sortie($tab2);
                $mois = substr($tab2['arDatePublication'], -8, 2);
                $annee = substr($tab2['arDatePublication'], 0, -8);
                //Si les mois ne sont pas les mêmes, on ferme la section
                if($moisVerif==$mois && $anneeVerif==$annee){
                    $mois_identique=true;
                }else{
                    $mois_identique=false;
                    echo '</section>';
                }
                if($mois_identique==false){
                    echo '<section>';
                    echo '<h2>',hm_afficher_date_article($tab2['arDatePublication']),'</h2>';
                }
                //Affichage
                echo '<article class="resume">';
                echo '<img src="', hm_url_image_illustration($tab2['arID']), '">';
                echo '<h3>',$tab2['arTitre'],'</h3>',
                    $tab2['arResume'];
                $id_crypt=hm_crypteSigneURL($tab2['arID']);
                echo '<footer><a href="../php/article.php?id=',$id_crypt,'">Lire l\'article</a></footer>',
                    '</article>';
                $moisVerif = substr($tab2['arDatePublication'], -8, 2);
                $anneeVerif = substr($tab2['arDatePublication'], 0, -8);
            }
    echo '</main>';
    // fermeture de la connexion à la base de données
    mysqli_close($bd);
}


?>