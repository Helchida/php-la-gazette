<?php
require_once('./bibli_gazette.php');
require_once('./bibli_generale.php');

// bufferisation des sorties
ob_start();

// démarrage de la session
session_start();

hm_verifie_authentification();
hm_verifie_authentification_redac();


if(isset($_POST['btnCreerArticle'])) {
    $erreurs=hml_traitement_creation_article();
}else{
    $erreurs=FALSE;
}

// génération de la page
hm_aff_entete('Nouvel article', 'Nouvel article');
hml_aff_creer_article($erreurs);

hm_aff_pied();

ob_end_flush(); //FIN DU SCRIPT

/**
 * Affiche zones pour creer un article
 * Zones: Titre, Résumé, Texte et Image
 *  
 * @param  array    $erreurs    Erreurs lors de l'envoie de l'ajout d'article'.
 */
function hml_aff_creer_article($erreurs){
    
    echo '<main>',
        '<section>',
            '<h2>Création d\'article</h2>',
            '<p>Veuillez écrire un texte, un titre et un résumé puis éventuellement choisir une photo pour publier un article.</p>'; 

    //Si presence d'erreurs alors on les affiches
    if($erreurs){
        echo '<div class="erreur">Les erreurs suivantes ont été relevées lors de la création de l\'article<ul>';
        foreach ($erreurs as $err) {
            echo '<li>', $err, '</li>';   
        }
        echo '</ul></div>';
    }
    echo '</section></main>';
    
    echo '<main>',
        '<section>',
            '<h2>Informations</h2>', 
            '<form enctype="multipart/form-data" action="nouveau.php" method="post">';

    echo '<p><fieldset class="cadre_titre"><legend>Ajoutez le titre</legend>',
            '<textarea id="titre" name="titre" rows="1" cols="100"></textarea>',
            '</fieldset></p>',        
        '<p><fieldset class="cadre_resume"><legend>Ajoutez le résumé</legend>',
            '<textarea id="resume" name="resume" rows="5" cols="100"></textarea>',
            '</fieldset></p>',        
        '<p><fieldset class="cadre_texte"><legend>Ajoutez le texte</legend>',
            '<textarea id="texte" name="texte" rows="40" cols="100"></textarea>',
        '</fieldset></p>',
        '<p><fieldset class="cadre_image"><legend>Ajoutez une image</legend>',
            '<input type="file" name="btnPhotoArticle">', 
        '</fieldset></p>',
        '<table>',
            '<tr>',
                '<td colspan="2">',
                    '<input type="submit" name="btnCreerArticle" value="Créer">',
                    '<input type="reset" value="Réinitialiser">', 
                '</td>',
            '</tr>',
        '</table>',   
     '</form>',
    '</section></main>';

}

/**
 *  Traitement d'une demande de création d'article'. 
 *  
 *  Si la création réussit, on insert les informations dans la table article, 
 * 
 *  @global array    $_POST
 *  @global array    $_SESSION
 *  @return array    un tableau contenant les erreurs s'il y en a
 */
function hml_traitement_creation_article(){
    $erreurs = array();

    $bd = hm_bd_connecter();
    //Requete qui recupere le nombre d'article
    $sql="SELECT COUNT(arID) as Nb_Article FROM article";
    $res=mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    $tab=mysqli_fetch_assoc($res);
    $tab=hm_html_proteger_sortie($tab);
    $arId=$tab['Nb_Article'];
    mysqli_free_result($res);

    //Requete qui recupere les id's des articles
    $sql="SELECT arID FROM article";
    $res=mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    while($tab2=mysqli_fetch_assoc($res)){
        $tab2=hm_html_proteger_sortie($tab2);
        if($arId==$tab2['arID']){
            $arId++;
        }
    }
    mysqli_free_result($res);

    // vérification des titre, résumé et texte
    $arTitre=htmlspecialchars(trim($_POST['titre']));
    $arResume=htmlspecialchars(trim($_POST['resume']));
    $arTexte=htmlspecialchars(trim($_POST['texte']));
    $arTitre=mysqli_real_escape_string($bd,$arTitre);
    $arResume=mysqli_real_escape_string($bd,$arResume);
    $arTexte=mysqli_real_escape_string($bd,$arTexte);
    hm_verifier_texte_article($arTitre, 'Le titre', $erreurs);
    hm_verifier_texte_article($arResume, 'Le résumé', $erreurs);
    hm_verifier_texte_article($arTexte, 'Le texte', $erreurs);
    
    // vérification de la photo
    $oks=array('.jpg');
    if(isset($_FILES['btnPhotoArticle']['name'])){
        if($_FILES['btnPhotoArticle']['name']!= NULL){
            $nomI=$_FILES['btnPhotoArticle']['name'];
            $ext=strtolower(substr($nomI,strrpos($nomI,'.')));
            if(in_array($ext,$oks)){
                $Dest='./../upload/'.strval($arId);
                if($_FILES['btnPhotoArticle']['error']==0 && @is_uploaded_file($_FILES['btnPhotoArticle']['tmp_name']) && @move_uploaded_file($_FILES['btnPhotoArticle']['tmp_name'],$Dest.$ext)){
                    
                }else{

                    $erreurs[]='Une erreur interne a empêché l\'upload de l\'image.';
                }
            }else{
                $erreurs[]='L\'extension de l\'image doit être .jpg.';
            }
        }
        
    }

    // si erreurs --> retour
    if (count($erreurs) > 0) {
        return $erreurs;   //===> FIN DE LA FONCTION
    }

 
    $annee=date('Y');
    $mois=date('m');
    $jour=date('d');
    $heure=date('H');
    $minute=date('i');
    $arAuteur=$_SESSION['user']['pseudo'];
    //Requete qui insert un nouvel article
    $sql="INSERT INTO article(arID, arTitre, arResume, arTexte, arDatePublication, arDateModification, arAuteur) 
        VALUES ('{$arId}','{$arTitre}','{$arResume}','{$arTexte}', '{$annee}{$mois}{$jour}{$heure}{$minute}',NULL, '{$arAuteur}')";
    mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    // fermeture de la connexion à la base de données
    mysqli_close($bd);
    
    // redirection sur la page protegee.php
    $id_crypt=hm_crypteSigneURL($arId);
    header('location: ./article.php?id='.$id_crypt);    // TODO : A MODIFIER DANS LE PROJET
    exit(); //===> Fin du script
}

