<?php
require_once('./bibli_gazette.php');
require_once('./bibli_generale.php');

// bufferisation des sorties
ob_start();

// démarrage de la session
session_start();

// Page accessible uniquement aux utilisateurs authentifiés
hm_verifie_authentification();

// ouverture de la connexion à la base 
$bd = hm_bd_connecter();
$sql = "SELECT * FROM utilisateur WHERE utPseudo = '{$_SESSION['user']['pseudo']}'";
$res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
$tab = mysqli_fetch_assoc($res);
$tab=hm_html_proteger_sortie($tab);
// Libération de la mémoire associée au résultat de la requête
mysqli_free_result($res);
if($tab['utStatut']==1 || $tab['utStatut']==3){
    $sql="SELECT * FROM utilisateur, redacteur WHERE utPseudo='{$_SESSION['user']['pseudo']}' AND rePseudo='{$_SESSION['user']['pseudo']}'";
    $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    $tab=mysqli_fetch_assoc($res);
    $tab=hm_html_proteger_sortie($tab);
    mysqli_free_result($res);
}


// si formulaire soumis, traitement de la demande d'inscription
$erreurInfo=FALSE;
$erreurMdp=FALSE;
$erreurBio=FALSE;
$erreurPhoto=FALSE;
//Si l'utilisateur clique sur le bouton Enregistrer (les infos)
if (isset($_POST['btnEnregistrerInfo'])) {
    $erreurInfo = hml_traitement_modifsInfo($tab,$bd);
    //Si l'utilisateur a le statut redacteur alors on récupere ses infos utilisateur et redacteur sinon juste celles utilisateur
    if($tab['utStatut']==1 || $tab['utStatut']==3){
        //Requete qui recupere infos redacteur et utilisateur de l'user
        $sql="SELECT * FROM utilisateur, redacteur WHERE utPseudo='{$_SESSION['user']['pseudo']}' AND rePseudo='{$_SESSION['user']['pseudo']}'";
        $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
        $tab=mysqli_fetch_assoc($res);
        $tab=hm_html_proteger_sortie($tab);
        // Libération de la mémoire associée au résultat de la requête
        mysqli_free_result($res);
    }else{
        //Requete qui recupere infos  utilisateur de l'user
        $sql = "SELECT * FROM utilisateur WHERE utPseudo = '{$_SESSION['user']['pseudo']}'";
        $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
        $tab = mysqli_fetch_assoc($res);
        $tab=hm_html_proteger_sortie($tab);
        // Libération de la mémoire associée au résultat de la requête
        mysqli_free_result($res);  
    }
//Si l'utilisateur clique sur le bouton Enregistrer (mot de passe)      
}else if(isset($_POST['btnEnregistrerMdp'])){
    $erreurMdp = hml_traitement_modifsMdp($bd);
//Si l'utilisateur clique sur le bouton Enregistrer (infos redacteur)    
}else if(isset($_POST['btnEnregistrerBio'])){
    $erreurBio=hml_traitement_modifsBio($bd);
    //Requete qui recupere infos redacteur et utilisateur de l'user
    $sql="SELECT * FROM utilisateur, redacteur WHERE utPseudo='{$_SESSION['user']['pseudo']}' AND rePseudo='{$_SESSION['user']['pseudo']}'";
    $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    $tab=mysqli_fetch_assoc($res);
    $tab=hm_html_proteger_sortie($tab);
    mysqli_free_result($res);
//Si l'utilisateur clique sur le bouton Valider (photo)
}else if(isset($_POST['btnValiderPhoto'])){
    $erreurPhoto=hml_traitement_modifsPhoto($bd);
}

// génération de la page
hm_aff_entete('Mon compte', 'Mon compte');


echo '<main>';
hml_aff_infos($erreurInfo,$tab);
hml_aff_mdp($erreurMdp);
//Si l'user a le statut redacteur on affiche les parties redacteur qu'il peut modifier
if($tab['utStatut']==1 || $tab['utStatut']==3){
    hml_aff_bio($erreurBio,$tab);
    hml_aff_photo($erreurPhoto,$tab);
}
echo '</main>';

// fermeture de la connexion à la base de données
mysqli_close($bd);
hm_aff_pied();

ob_end_flush();

/**
 * Contenu de la page : affichage du formulaire de modification des infos
 *
 * En absence de soumission, $erreurInfo est égal à FALSE
 * Quand l'inscription échoue, $erreurInfo est un tableau de chaînes  
 *
 *  @param mixed    $erreurInfo     Erreurs lors de l'envoie de la page
 *  @param mixed    $tab            Informations actuelles concernant l'utilisateur
 *  @global array   $_POST
 */
function hml_aff_infos($erreurInfo,$tab){
    $anneeCourante = (int) date('Y');

    echo '<section>',
        '<h2>Informations personnelles</h2>',
        '<p>Vous pouvez modifier les informations suivantes.</p>',            
        '<form action="compte.php" method="post">';
    //Si erreurs de saisie dans la modification alors on affiche des erreurs sinon message de validité
    if ($erreurInfo) {
        echo '<div class="erreur">Les erreurs suivantes ont été relevées :<ul>';
        foreach ($erreurInfo as $err) {
            echo '<li>', $err, '</li>';   
        }
        echo '</ul></div>';
    }else if (isset($_POST['btnEnregistrerInfo'])) {
        echo '<div class="reussite">Les modifications ont été effectuées.</div>';
    } 

    echo '<table>';
    //Initialisation d'informations et affichage
    $civilite=$tab['utCivilite'];
    if($civilite=="h"){
        $civilite=1;
    }else{
        $civilite=2;
    }
    $date=(string)$tab['utDateNaissance'];
    $jour=(int)($date[6].$date[7]);
    $mois=(int)($date[4].$date[5]);
    $annee=(int)($date[0].$date[1].$date[2].$date[3]);
    hm_aff_ligne_input_radio('Votre civilité :', 'radSexe', array(1 => 'Monsieur', 2 => 'Madame'), $civilite, array('required' => 0));
    hm_aff_ligne_input('text', 'Votre nom :', 'nom', $tab['utNom'], array('required' => 0));
    hm_aff_ligne_input('text', 'Votre prénom :', 'prenom', $tab['utPrenom'], array('required' => 0));
    hm_aff_ligne_date('Votre date de naissance :', 'naissance', $anneeCourante - NB_ANNEE_DATE_NAISSANCE + 1, $anneeCourante, $jour, $mois, $annee);
    hm_aff_ligne_input('email', 'Votre email :', 'email', $tab['utEmail'], array('required' => 0));
    echo    '<tr>', '<td colspan="2">';
    hm_aff_input_checkbox('J\'accepte de recevoir des tonnes de mails pourris', 'cbSpam', $tab['utMailsPourris']);

    echo   '<tr>',
                '<td colspan="2">',
                    '<input type="submit" name="btnEnregistrerInfo" value="Enregistrer">',
                    '<input type="reset" value="Réinitialiser">', 
                '</td>',
            '</tr>',
        '</table>',
        '</form>',
        '</section>' ;
}

/**
 * Contenu de la page : affichage du formulaire de modification du mot de passe
 *
 * En absence de soumission, $erreurMdp est égal à FALSE
 * Quand l'inscription échoue, $erreurMdp est un tableau de chaînes  
 *
 *  @param mixed    $erreurMdp      Erreurs lors de l'envoie de la page concernant le mot de passe
 */
function hml_aff_mdp($erreurMdp){
    echo '<section>',
        '<h2>Authentification</h2>',
        '<p>Vous pouvez modifier votre mot de passe ci-dessous.</p>',            
        '<form action="compte.php" method="post">';
    if($erreurMdp==FALSE){
    //Si  mot de passe vide
    }else if($erreurMdp[0]==1) {
        
        echo '<div class="erreur">L\'erreur suivante a été relevée :<ul>';
        echo '<li>Les mots de passe ne doivent pas être vides.</li>';   
        echo '</ul></div>';
    //Si  2 mot de passes non identiques
    }else if($erreurMdp[0]==2){
        echo '<div class="erreur">L\'erreur suivante a été relevée :<ul>';
        echo '<li>Les mots de passe doivent être identiques.</li>';   
        echo '</ul></div>';
    //Si tout va bien
    }else if($erreurMdp[0]==3){
        echo '<div class="reussite">Le mot de passe a été changé avec succès.</div>';
    }
    //Affichage champs mot de passe
    echo '<table>';
    hm_aff_ligne_input('password', 'Choisissez un mot de passe :', 'passe1', '', array('required' => 0));
    hm_aff_ligne_input('password', 'Répétez le mot de passe :', 'passe2', '', array('required' => 0));
    
    echo   '<tr>',
                '<td colspan="2">',
                    '<input type="submit" name="btnEnregistrerMdp" value="Enregistrer">',
                '</td>',
            '</tr>',
        '</table>',
        '</form>',
        '</section>' ;
}

/**
 * Contenu de la page : affichage du formulaire de modification des infos rédacteur de l'utilisateur
 *
 * En absence de soumission, $erreurBio est égal à FALSE
 * Quand l'inscription échoue, $erreurBio est un tableau de chaînes  
 *
 *  @param mixed    $erreurBio      Erreurs lors de l'envoie de la page concernant infos rédacteur
 *  @param mixed    $tab            Informations actuelles concernant l'utilisateur
 *  @global array   $_POST
 */
function hml_aff_bio($erreurBio,$tab){
    echo '<section>',
        '<h2>Informations rédacteur</h2>',
        '<p>Vous pouvez modifier les informations suivantes.</p>',            
        '<form action="compte.php" method="post">';
        //Si erreurs de saisie dans la modification alors on affiche des erreurs sinon message de validité
        if ($erreurBio) {
        echo '<div class="erreur">Les erreurs suivantes ont été relevées :<ul>';
        foreach ($erreurBio as $err) {
            echo '<li>', $err, '</li>';   
        }
        echo '</ul></div>';
    }else if (isset($_POST['btnEnregistrerBio'])) {
        echo '<div class="reussite">Les modifications ont été effectuées.</div>';
    }
    echo '<table>';
    //Choix Bio
    hm_aff_ligne_input('text', 'Votre biographie :', 'bio', $tab['reBio'], array('required' => 0));
    
    
    //Choix Categorie
    echo '<tr>',
            '<td>Votre categorie :</td><td>';

    hm_aff_liste('categorie',array(1=>'Rédacteur en chef',2=>'Premier violon',3=>'Sous-fifre'),$tab['reCategorie']);
    
    
    echo '</td></tr>'; 
    //Choix Fonction

    hm_aff_ligne_input('text', 'Votre fonction :', 'fonction', $tab['reFonction']);
    
    echo   '<tr>',
                '<td colspan="2">',
                    '<input type="submit" name="btnEnregistrerBio" value="Enregistrer">',
                    '<input type="reset" value="Réinitialiser">', 
                '</td>',
            '</tr>',
        '</table>',
        '</form>',
        '</section>' ;
}

/**
 * Contenu de la page : affichage de la photo de l'utilisateur et du choix de photo
 *
 * En absence de soumission, $erreurPhoto est égal à FALSE
 * Quand l'inscription échoue, $erreurPhoto est un tableau de chaînes  
 *
 *  @param mixed    $erreurPhoto      Erreurs lors de l'envoie de la page concernant infos rédacteur
 *  @param mixed    $tab            Informations actuelles concernant l'utilisateur
 *  @global array   $_POST
 */
function hml_aff_photo($erreurPhoto,$tab){
    echo '<section>',
    '<h2>Photo</h2>',
    '<p>Vous pouvez choisir une photo en cliquant sur le bouton Parcourir.</p>',            
    '<form enctype="multipart/form-data" action="compte.php" method="post">';
    //Si erreurs de saisie dans la modification alors on affiche des erreurs sinon message de validité
    if ($erreurPhoto) {
        echo '<div class="erreur">Les erreurs suivantes ont été relevées :<ul>';
        foreach ($erreurPhoto as $err) {
            echo '<li>', $err, '</li>';   
        }
        echo '</ul></div>';
    }else if (isset($_POST['btnEnregistrerPhoto'])) {
        echo '<div class="reussite">Les modifications ont été effectuées.</div>';
    }
    echo '<table>';
    echo '<div class="imgRedacteur">';
    
    //Si image existante on l'affiche sinon on affiche anonyme.jpg
    if(is_file("../upload/{$tab['utPseudo']}.jpg")){
        $imgFile = "../upload/{$tab['utPseudo']}.jpg";
        echo '<img class="imgRedacteur" src="',$imgFile,'" width="150" height="200">';
    }else{
        echo '<img class="imgRedacteur" src="../images/anonyme.jpg">';
    }
    
    echo '</div>';
    
    echo '<tr>',
        '<td colspan="2">',
            '<input type="file" name="btnEnregistrerPhoto"><br><br>', 
            '<input type="submit" name="btnValiderPhoto" value="Valider">',
        '</td>',
    '</tr>',
        '</table>',
        '</form>',
        '</section>' ;

    
}

/**
 *  Traitement d'une demande de modification d'informations d'utilisateur. 
 *  
 *  Si la modification réussit, on update les informations dans la table utilisateur, 
 *  
 *  @param  array    $tab2      Infos actuelles de l'utilisateur
 *  @param  object   $bd        Ouverture base de donnee
 *  @global array    $_POST
 *  @return array    un tableau contenant les erreurs s'il y en a
 */
function hml_traitement_modifsInfo($tab2,$bd){
    $erreurs = array();

    // vérification de la civilité
    if (! isset($_POST['radSexe'])){
        $erreurs[] = 'Vous devez choisir une civilité.';
    }
    else if (! (hm_est_entier($_POST['radSexe']) && hm_est_entre($_POST['radSexe'], 1, 2))){
        hm_session_exit(); 
    }

    // vérification des noms et prénoms
    $nom = trim($_POST['nom']);
    $prenom = trim($_POST['prenom']);
    hml_verifier_texte_nom_prenom($nom, 'Le nom', $erreurs, LMAX_NOM);
    hml_verifier_texte_nom_prenom($prenom, 'Le prénom', $erreurs, LMAX_PRENOM);

    //verif date
    $jour = (int)$_POST['naissance_j'];
    $mois = (int)$_POST['naissance_m'];
    $annee = (int)$_POST['naissance_a'];
    if (!checkdate($mois, $jour, $annee)) {
        $erreurs[] = 'La date de naissance n\'est pas valide.';
    }
    else if (mktime(0,0,0,$mois,$jour,$annee+18) > time()) {
        $erreurs[] = 'Vous devez avoir au moins 18 ans pour vous inscrire.'; 
    }

    // vérification du format de l'adresse email
    $email = trim($_POST['email']);
    if (empty($email)){
        $erreurs[] = 'L\'adresse mail ne doit pas être vide.'; 
    }
    else if (mb_strlen($email, 'UTF-8') > LMAX_EMAIL){
        $erreurs[] = 'L\'adresse mail ne peut pas dépasser '.LMAX_EMAIL.' caractères.';
    }
    // la validation faite par le navigateur en utilisant le type email pour l'élément HTML input
    // est moins forte que celle faite ci-dessous avec la fonction filter_var()
    // Exemple : 'l@i' passe la validation faite par le navigateur et ne passe pas
    // celle faite ci-dessous
    else if(! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $erreurs[] = 'L\'adresse mail n\'est pas valide.';
    }

    // vérification si l'utilisateur accepte de recevoir les mails pourris
    /*if (isset($_POST['cbSpam']) && ! (hm_est_entier($_POST['cbSpam']) && $_POST['cbSpam'] == 1)){
        hm_session_exit(); 
    }*/

    // si erreurs --> retour
    if (count($erreurs) > 0) {
        return $erreurs;   //===> FIN DE LA FONCTION
    }

    
    // vérification de l'existence de l'email
    $emaile = mysqli_real_escape_string($bd, $email);
    //Requete qui recupere l'email de l'utilisateur
    $sql = "SELECT utEmail FROM utilisateur WHERE utEmail = '{$emaile}'";
    $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    
    while($tab = mysqli_fetch_assoc($res)) {
        $tab=hm_html_proteger_sortie($tab);
        if (($tab['utEmail'] == $email) && ($tab2['utEmail']!=$email)){
            $erreurs[] = 'Cette adresse email est déjà inscrite.';
        }
    }
    // Libération de la mémoire associée au résultat de la requête
    mysqli_free_result($res);

    // si erreurs --> retour
    if (count($erreurs) > 0) {
        // fermeture de la connexion à la base de données
        mysqli_close($bd);
        return $erreurs;   //===> FIN DE LA FONCTION
    }

    if ($mois < 10) {
        $mois = '0' . $mois;   
    }
    if ($jour < 10) {
        $jour = '0' . $jour;   
    }
    $civilite = (int) $_POST['radSexe'];
    $civilite = $civilite == 1 ? 'h' : 'f';
    
    $mailsPourris = isset($_POST['cbSpam']) ? 1 : 0;
    
    $nom = mysqli_real_escape_string($bd, $nom);
    $prenom = mysqli_real_escape_string($bd, $prenom);
    
    //Requete qui update les infos de l'utilisateur
    $sql = "UPDATE utilisateur SET utEmail='{$emaile}', utNom='{$nom}', utPrenom='{$prenom}',
             utDateNaissance={$annee}{$mois}{$jour}, utCivilite='{$civilite}', utMailsPourris='{$mailsPourris}' 
            WHERE utPseudo='{$_SESSION['user']['pseudo']}'";

    mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    

}

/**
 *  Traitement d'une demande de modification de mot de passe de l'utilisateur. 
 *  
 *  Si la modification réussit, on update le mot de passe dans la table utilisateur, 
 *  
 *  @param  object   $bd        Ouverture base de donnee
 *  @global array    $_POST
 *  @global array    $_SESSION
 *  @return array    un tableau contenant les erreurs s'il y en a
 */
function hml_traitement_modifsMdp($bd){
    $erreurs = array();
    // vérification des mots de passe
    $passe1 = trim($_POST['passe1']);
    $passe2 = trim($_POST['passe2']);
    if (empty($passe1) || empty($passe2)) {
        $erreurs[0] = 1;
    }
    else if ($passe1 !== $passe2) {
        $erreurs[0] = 2;
    }

    // si erreurs --> retour
    if (count($erreurs) > 0) {
        return $erreurs;   //===> FIN DE LA FONCTION
    }

    // calcul du hash du mot de passe pour enregistrement dans la base.
    $passe = password_hash($passe1, PASSWORD_DEFAULT);
    $passe = mysqli_real_escape_string($bd, $passe);

    $sql = "UPDATE utilisateur SET utPasse='{$passe}' WHERE utPseudo='{$_SESSION['user']['pseudo']}'";
    $erreurs[0]=3;    
    mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    return $erreurs;
}

/**
 *  Traitement d'une demande de modification des informations redacteur de l'utilisateur. 
 *  
 *  Si la modification réussit, on update les informations dans la table redacteur, 
 *  
 *  @param  object   $bd        Ouverture base de donnee
 *  @global array    $_POST
 *  @global array    $_SESSION
 *  @return array    un tableau contenant les erreurs s'il y en a
 */
function hml_traitement_modifsBio($bd){
    $erreurs=array();
    $bio = hm_html_proteger_sortie(trim($_POST['bio']));
    $cat=hm_html_proteger_sortie($_POST['categorie']);
    $fonction = hm_html_proteger_sortie(trim($_POST['fonction']));
    hml_verifier_texte_bio($bio, 'La biographie', $erreurs);
    hml_verifier_texte_fonction($fonction, 'La fonction', $erreurs);
    // si erreurs --> retour
    if (count($erreurs) > 0) {
        return $erreurs;   //===> FIN DE LA FONCTION
    }
    $bio = mysqli_real_escape_string($bd, $bio);
    $fonction = mysqli_real_escape_string($bd, $fonction);
    //Requete qui update les infos d'un redacteur
    $sql = "UPDATE redacteur SET reBio='{$bio}', reCategorie='{$cat}', reFonction='{$fonction}' 
            WHERE rePseudo='{$_SESSION['user']['pseudo']}'";

    mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
}

/**
 *  Traitement d'une demande de modification de photo de l'utilisateur rédacteur. 
 *  
 *  Si la modification réussit, on update la photo dans le fichier upload,
 *  Photo enregistré: format jbigoude.jpg pour Johnny Bigoude 
 *  
 *  @global array    $_POST
 *  @global array    $_SESSION
 *  @return array    un tableau contenant les erreurs s'il y en a
 */
function hml_traitement_modifsPhoto(){
    $erreurs=array();

    $oks=array('.jpg');
    //Modification de la photo de redacteur
    if(isset($_FILES['btnEnregistrerPhoto']['name'])){
        $nomI=$_FILES['btnEnregistrerPhoto']['name'];
        $ext=strtolower(substr($nomI,strrpos($nomI,'.')));
        if(in_array($ext,$oks)){
            $Dest='./../upload/'.$_SESSION['user']['pseudo'];
            if($_FILES['btnEnregistrerPhoto']['error']==0 && @is_uploaded_file($_FILES['btnEnregistrerPhoto']['tmp_name']) && @move_uploaded_file($_FILES['btnEnregistrerPhoto']['tmp_name'],$Dest.$ext)){
                
            }else{
                $erreurs[]='Une erreur interne a empêché l\'upload de l\'image.';
            }
        }else{
            $erreurs[]='L\'extension de l\'image doit être .jpg.';
        }
    }else{
        $erreurs[]='Il faut choisir une image pour l\'uploader.';
    }

    return $erreurs;
  
}

//___________________________________________________________________
/**
 * Vérification des champs nom et prénom
 *
 * @param  string       $texte champ à vérifier
 * @param  string       $nom chaîne à ajouter dans celle qui décrit l'erreur
 * @param  array        $erreurs tableau dans lequel les erreurs sont ajoutées
 * @param  int          $long longueur maximale du champ correspondant dans la base de données
 */
function hml_verifier_texte_nom_prenom($texte, $nom, &$erreurs, $long = -1){
    mb_regex_encoding ('UTF-8'); //définition de l'encodage des caractères pour les expressions rationnelles multi-octets
    if (empty($texte)){
        $erreurs[] = "$nom ne doit pas être vide.";
    }
    else if(strip_tags($texte) != $texte){
        $erreurs[] = "$nom ne doit pas contenir de tags HTML";
    }
    elseif ($long > 0 && mb_strlen($texte, 'UTF-8') > $long){
        // mb_* -> pour l'UTF-8, voir : https://www.php.net/manual/fr/function.mb-strlen.php
        $erreurs[] = "$nom ne peut pas dépasser $long caractères";
    }
    elseif(!mb_ereg_match('^[[:alpha:]]([\' -]?[[:alpha:]]+)*$', $texte)){
        $erreurs[] = "$nom contient des caractères non autorisés";
    }
}

/**
 * Vérification du champs texte biographie
 *
 * @param  string       $texte champ à vérifier
 * @param  string       $nom chaîne à ajouter dans celle qui décrit l'erreur
 * @param  array        $erreurs tableau dans lequel les erreurs sont ajoutées
 */
function hml_verifier_texte_bio($texte, $nom, &$erreurs){
    mb_regex_encoding ('UTF-8'); //définition de l'encodage des caractères pour les expressions rationnelles multi-octets
    if (empty($texte)){
        $erreurs[] = "$nom ne doit pas être vide.";
    }
    else if(strip_tags($texte) != $texte){
        $erreurs[] = "$nom ne doit pas contenir de tags HTML";
    }
    
}

/**
 * Vérification du champs texte fonction
 *
 * @param  string       $texte champ à vérifier
 * @param  string       $nom chaîne à ajouter dans celle qui décrit l'erreur
 * @param  array        $erreurs tableau dans lequel les erreurs sont ajoutées
 */
function hml_verifier_texte_fonction($texte, $nom, &$erreurs){
    mb_regex_encoding ('UTF-8'); //définition de l'encodage des caractères pour les expressions rationnelles multi-octets
    if(strip_tags($texte) != $texte){
        $erreurs[] = "$nom ne doit pas contenir de tags HTML";
    }
    
}
?>