<?php
require_once('./bibli_gazette.php');
require_once('./bibli_generale.php');

// bufferisation des sorties
ob_start();

// démarrage de la session
session_start();

hm_verifie_authentification();
// ouverture de la connexion à la base 
$bd = hm_bd_connecter();
//Requete qui recupere le nombre de commentaire ecrit, le nombre d'article ecrit, le statut de chaque pseudo.
$sql="SELECT COUNT(coID) as Nombre_Commentaire, COUNT(arID) as Nombre_Article,utPseudo, utStatut FROM commentaire RIGHT JOIN utilisateur ON coAuteur=utPseudo LEFT JOIN article ON arAuteur=utPseudo GROUP BY utPseudo";
$res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);

//Requete qui recupere la Moyenne de commentaire recu pour chaque pseudo.
$sql="SELECT utPseudo ,COUNT(coID)/ COUNT( DISTINCT ArID) as Moyenne FROM article LEFT OUTER JOIN commentaire ON arID = coArticle RIGHT OUTER JOIN utilisateur ON arAuteur=utPseudo GROUP BY utPseudo";
$res2 = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);

// Page accessible uniquement aux utilisateurs authentifiés
hm_verifie_authentification_admin();

//Si l'utilisateur clique sur modifier
if (isset($_POST['btnModifier'])) {
    hml_traitement_modifsDroits($bd,$res);
    mysqli_free_result($res);
    //Requete qui recupere le nombre de commentaire ecrit, le nombre d'article ecrit, le statut de chaque pseudo.
    $sql="SELECT COUNT(coID) as Nombre_Commentaire, COUNT(arID) as Nombre_Article,utPseudo, utStatut FROM commentaire RIGHT JOIN utilisateur ON coAuteur=utPseudo LEFT JOIN article ON arAuteur=utPseudo GROUP BY utPseudo";
    $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);

}

// génération de la page
hm_aff_entete('Administration', 'Administration');
hml_aff_droits($res,$res2);

mysqli_free_result($res);
mysqli_free_result($res2);
//Fermeture de la connexion à la base de données
mysqli_close($bd);
hm_aff_pied();

ob_end_flush();

/**
 * Affiche les droits des utilisateurs ainsi que le nombre de commentaire mis,
 * le nombre d'article écrits et le nombre moyen de commentaire reçu.
 *
 * @param bool    $res        Resultat requete 1
 * @param bool    $res2       Resultat requete 2
 */
function hml_aff_droits($res,$res2){
    echo '<main>';
    echo '<section>',
        '<h2>Droits des utilisateurs</h2>',
        '<p>Vous pouvez modifier les droits des utilisateurs.</p>',            
        '<form action="administration.php" method="post">';

    //Si l'utilisateur à cliquer sur modifier
    if (isset($_POST['btnModifier'])) {
        echo '<div class="reussite">Les modifications ont été effectuées.</div>';
    }
        
    echo '<table>';
    //Parcours des tableaux Nombre_Commentaire_Nombre_Article et Moyennes (contenants tous les utilisateurs dans le même ordre)
    while(($tab_NbComm_NbArt=mysqli_fetch_row($res)) && ($tab_moyennes=mysqli_fetch_assoc($res2))){
        $tab_NbComm_NbArt = hm_html_proteger_sortie($tab_NbComm_NbArt);
        $tab_moyennes = hm_html_proteger_sortie($tab_moyennes);
        echo '<p><h3>',$tab_NbComm_NbArt[2],' :</h3> ',$tab_NbComm_NbArt[0],' commentaire(s) écrit(s).<br> ',$tab_NbComm_NbArt[1],' article(s) écrit(s).<br>';
            //Affichage 
            if($tab_moyennes['Moyenne']==NULL || $tab_moyennes['Moyenne']==0.0000){
                echo 'Moyenne de commentaire recu par article écrit : 0<br>';
            }else{
                echo 'Moyenne de commentaire recu par article écrit : ',$tab_moyennes['Moyenne'],'<br>';
            }

            $name=$tab_NbComm_NbArt[2];
            hm_aff_liste($name,array(0=>'Utilisateur simple',1=>'Rédacteur',2=>'Administrateur',3=>'Rédacteur et Administrateur'),$tab_NbComm_NbArt[3]);

            echo '<br><br></p>';
    }
    echo   '<tr>',
                '<td colspan="2">',
                    '<input type="submit" name="btnModifier" value="Modifier">',
                    '<input type="reset" value="Réinitialiser">', 
                '</td>',
            '</tr></table>';
    echo '</form>',
        '</section>' ,
        '</main>';
}

/**
 * Modifie les droits dans la base de donnee selon les choix de l'utilisateur.
 *
 * @param object    $bd        Ouverture base de donnee
 * @param bool    $res       Resultat requete
 */
function hml_traitement_modifsDroits($bd,$res){
    //Parcours le tableau Nombre_Commentaire_Nombre_Article
    while($tab_NbComm_NbArt=mysqli_fetch_row($res)){
        $tab_NbComm_NbArt = hm_html_proteger_sortie($tab_NbComm_NbArt);
        $pseudo=$tab_NbComm_NbArt[2];
        $newStatut=hm_html_proteger_sortie($_POST[$pseudo]);
        //Si le nouveau statut choisis pour un pseudo est different de l'ancien statut alors on va le modifier sinon on ne fait rien 
        if($newStatut!=$tab_NbComm_NbArt[3]){
            //Requete qui update le statut d'un utilisateur
            $sql = "UPDATE utilisateur SET utStatut='{$newStatut}'
            WHERE utPseudo='{$pseudo}'";
            mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
            //Requet qui selectionne le pseudo redacteur correspondant au pseudo utilisateur
            $sql="SELECT rePseudo FROM redacteur where rePseudo='{$pseudo}'";
            $res3= mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
            $tab2=mysqli_fetch_assoc($res3);
            $tab2=hm_html_proteger_sortie($tab2);
            mysqli_free_result($res3);

            
            //Si le nouveau statut est egale au droit redacteur et l'ancien statut n'as pas le droit redacteur et que l'utilisateur n'est pas dans la table redacteur,
            // on rentre dans le if sinon on ne fait rien?
            if(!isset($tab2)){
                if(($newStatut==1 || $newStatut==3) && ($tab_NbComm_NbArt[3]==2 ||$tab_NbComm_NbArt[3]==0)){
                    //Requete qu insert un nouveau redacteur dans la table.
                    $sql="INSERT INTO redacteur(rePseudo, reBio, reCategorie, reFonction) 
                    VALUES ('{$pseudo}','',3, NULL)";
                    mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
                }
            }
            
        }

    }
}
?>