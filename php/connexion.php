<?php

require_once('./bibli_gazette.php');
require_once('./bibli_generale.php');

// bufferisation des sorties
ob_start();

// démarrage de la session
session_start();

// si l'utilisateur est déjà authentifié
if (isset($_SESSION['user'])){
    header ('location: ../index.php');
    exit();
}

// si formulaire soumis, traitement de la demande d'inscription
if (isset($_POST['btnConnexion'])) {
    $err = hml_traitement_connexion();
}
else{
    $err = FALSE;
}

// génération de la page
hm_aff_entete('Connexion', 'Connexion');
hml_aff_connex($err);
hm_aff_pied();

ob_end_flush(); //FIN DU SCRIPT

/**
 * Affiche le menu de connexion
 * 
 * Demande un pseudo et un mot de passe
 * Affiche éventuellement erreurs de connexion
 * 
 * @param   mixed      $err    Tableau d'erreurs, false sinon
 */
function hml_aff_connex($err) {
    $pseudo = '';

    echo '<main>',
    '<section>',
        '<h2>Formulaire de connexion</h2>',
        '<p>Pour vous identifier, remplissez le formulaire ci-dessous.</p>',            
        '<form action="connexion.php" method="post">';
    //Si présence d'erreurs on les affiches
    if ($err) {
        echo '<div class="erreur">Les erreurs suivantes ont été relevées lors de votre connexion :<ul>';
        foreach ($err as $e) {
            echo '<li>', $e, '</li>';   
        }
        echo '</ul></div>';
    }    

    echo '<table>';
    hm_aff_ligne_input('text', 'Pseudo', 'pseudo', $pseudo, array('required' => 0));
    hm_aff_ligne_input('password', 'Mot de passe', 'passe', '', array('required' => 0));

    echo   '<tr>',
                '<td colspan="2">',
                    '<input type="submit" name="btnConnexion" value="Se connecter">',
                    '<input type="reset" value="Annuler">', 
                '</td>',
            '</tr>',
        '</table>',
        '</form>',
            '<p>', 
                'Pas encore inscrit ? N\'attendez pas, <a href="inscription.php">inscrivez-vous</a> !',
            '</p>', 
        '</section></main>';

    
}

/**
 *  Traitement d'une demande de connexion. 
 *  
 *  Si la connexion réussit, 
 *  la variable de session $_SESSION['user'] est créée et l'utilisateur est redirigé vers la
 *  page index.php
 *
 *  @global array    $_POST
 *  @global array    $_SESSION
 *  @return array    un tableau contenant les erreurs s'il y en a
 */
function hml_traitement_connexion() {
    /*
    * Toutes les erreurs détectées qui nécessitent une modification du code HTML sont considérées comme des tentatives de piratage 
    * et donc entraînent l'appel de la fonction hm_session_exit() sauf les éventuelles suppressions des attributs required 
    * car l'attribut required est une nouveauté apparue dans la version HTML5 et nous souhaitons que l'application fonctionne également 
    * correctement sur les vieux navigateurs qui ne supportent pas encore HTML5
    *
    */
    /*if( !hm_parametres_controle('post', array('pseudoCo', 'passeCo', 'btnConnexion'))) {
        hm_session_exit();   
    }*/
    $err = array();

    // vérification du pseudo
    $pseudoCo = trim($_POST['pseudo']);
    if (empty($pseudoCo)) {
        $err[] = 'Le pseudo ne doit pas être vide.';
    }
    
   
    // vérification du mot de passe
    $passeCo = trim($_POST['passe']);
    if (empty($passeCo)) {
        $err[] = 'Le mot de passe ne doit pas être vide.';
    }

    // si erreurs --> retour
    if (count($err) > 0) {
        return $err;   //===> FIN DE LA FONCTION
    }

    // ouverture de la connexion à la base 
    $bd = hm_bd_connecter();
    $mdp_hash=password_hash($passeCo, PASSWORD_DEFAULT);
    $mdp_hash = mysqli_real_escape_string($bd, $mdp_hash);
    
    $sql = "SELECT * FROM utilisateur WHERE utPseudo = '$pseudoCo'";
    $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    $tab = mysqli_fetch_assoc($res);
    if(mysqli_num_rows($res)==0){
        $err[] = 'Pseudo non valide.';
    }else{
        if(password_verify($passeCo,$tab['utPasse'])){
            
        }else{
            $err[] = 'Mot de passe non valide';
        }
    }

    // Libération de la mémoire associée au résultat de la requête
    mysqli_free_result($res);

    // si erreurs --> retour
    if (count($err) > 0) {
        // fermeture de la connexion à la base de données
        mysqli_close($bd);
        return $err;   //===> FIN DE LA FONCTION
    }

    if($tab['utStatut']==0){
        $_SESSION['user'] = array('pseudo' => $pseudoCo, 'redacteur' => false, 'administrateur' => false);
    }
    if($tab['utStatut']==1){
        $_SESSION['user'] = array('pseudo' => $pseudoCo, 'redacteur' => true, 'administrateur' => false);
    }
    if($tab['utStatut']==2){
        $_SESSION['user'] = array('pseudo' => $pseudoCo, 'redacteur' => false, 'administrateur' => true);
    }
    if($tab['utStatut']==3){
        $_SESSION['user'] = array('pseudo' => $pseudoCo, 'redacteur' => true, 'administrateur' => true);
    }

    
    // fermeture de la connexion à la base de données
    mysqli_close($bd);
    
    // redirection sur la page index.php
    header('location: ./../index.php');
    exit(); //===> Fin du script
}
    


?>
