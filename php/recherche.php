<?php
require_once('./bibli_gazette.php');
require_once('./bibli_generale.php');

// bufferisation des sorties
ob_start();

// démarrage de la session
session_start();

// affichage de l'entête
hm_aff_entete('Recherche', 'Recherche');
hml_aff_recherche();
hm_aff_pied();

ob_end_flush();

/**
 * Affiche les articles recherchés ainsi que la barre de recherche
 *  
 * @global array    $_POST
 */
function hml_aff_recherche(){
    
    //Si l'utilisateur a cliqué sur le bouton Rechercher
    if(isset($_POST['btnRechercher'])){
        $textRech=hm_html_proteger_sortie($_POST['textRech']);
        $tab_mots=explode(" ",$textRech);
        $motCourt=false;
        foreach($tab_mots as $value){
            if(strlen($value)<3){
                $motCourt=true;
            }
            $tab_mot_minuscule[]=strtolower($value);
        }
    }else{
        $textRech="";
        $tab_mots=NULL;
        $tab_mot_minuscule[]=NULL;
        $motCourt=false;
    }
    echo '<main>',
            '<section>',
                '<h2>Rechercher des articles</h2>',
                '<p>Les critères de recherche doivent faire au moins 3 caractères pour être pris en compte.</p>',
                '<form action="recherche.php" method="post">',
                '<table>',
                    '<tr>',
                        '<td colspan="2">',
                            '<input type="text" name="textRech" value="',$textRech,'"size="40">',
                            '<input type="submit" name="btnRechercher" value="Rechercher">',
                        '</td>',
                    '</tr>',
                '</table>',
                '</form>';
                if ($motCourt) {
                    echo '<div class="erreur">Un mot contient moins de 3 lettres.';
                    echo '</div>';
                }
            echo '</section>';
    if (!$motCourt) {
        $bd = hm_bd_connecter();
        //Requete qui recupere les informations d'un article trié par ordre decroissant selon la date de publi
        $sql="SELECT arID, arTitre, arResume, arDatePublication FROM article ORDER BY arDatePublication DESC";
        $res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
        
        $mois_identique=false;
        $moisVerif='';
        $anneeVerif=0;
        $articlePresent=false;
        //Parcours des articles
        while($tab=mysqli_fetch_assoc($res)){
            $tab=hm_html_proteger_sortie($tab);
            $mois = substr($tab['arDatePublication'], -8, 2);
            $annee = substr($tab['arDatePublication'], 0, -8);
            //Si les mois ne sont pas les mêmes, on ferme la section
            if($moisVerif==$mois && $anneeVerif==$annee){
                $mois_identique=true;
            }else{
                $mois_identique=false;
                echo '</section>';
            }
            $compteurPrésence=0;
            $titre_minuscule=strtolower($tab['arTitre']);
            $resume_minuscule=strtolower($tab['arResume']);
            foreach($tab_mot_minuscule as $val){
            //Verification si présence mots    
            if((strpos($titre_minuscule,$val) !== FALSE) || (strpos($resume_minuscule,$val) !== FALSE)){
                $compteurPrésence++;
            } 
            }
            if($compteurPrésence==count($tab_mot_minuscule)){
                $articlePresent=true;
                //Affiche article qui contient les mots
                if($mois_identique==false){
                    echo '<section>';
                    echo '<h2>',hm_afficher_date_article($tab['arDatePublication']),'</h2>';
                }
                
                echo '<article class="resume">';
                echo '<img src="', hm_url_image_illustration($tab['arID']), '">';
                echo '<h3>',$tab['arTitre'],'</h3>',
                    $tab['arResume'];
                $id_crypt=hm_crypteSigneURL($tab['arID']);
                echo '<footer><a href="../php/article.php?id=',$id_crypt,'">Lire l\'article</a></footer>',
                    '</article>';
                
                $moisVerif = substr($tab['arDatePublication'], -8, 2);
                $anneeVerif = substr($tab['arDatePublication'], 0, -8);   
            }
            
        }
        //Si 0 article alors on le dit
        if(!$articlePresent){
            if(isset($_POST['btnRechercher'])){
                echo '<section><p>Aucun article contenant cette chaine n\'existe.</p></section>';
            }
        }
        // libération des ressources
        mysqli_free_result($res);
        // fermeture de la connexion à la base de données
        mysqli_close($bd);
    }
    echo '</main>';
    
    
}


?>