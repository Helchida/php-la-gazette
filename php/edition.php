<?php
require_once('./bibli_gazette.php');
require_once('./bibli_generale.php');

// bufferisation des sorties
ob_start();

// démarrage de la session
session_start();

hm_verifie_authentification();
//ouverture de la base de donnée
$bd = hm_bd_connecter();
hm_verifie_authentification_page_redac($bd);

$erreurs=false;
$btnOui=false;
//Si l'utilisateur a cliqué sur le bouton Modifier
if(isset($_POST['btnModifierArticle'])) {
    $erreurs=hml_traitement_modifier_article($bd);
//Si l'utilisateur a cliqué sur le bouton Supprimer
}else if(isset($_POST['btnSupprimerArticle'])){
    $btnOui=true;
//Si l'utilisateur a cliqué sur le bouton Oui
}else if(isset($_POST['btnOui'])){
    hml_traitement_supprimer_article($bd);
//Si l'utilisateur a cliqué sur le bouton Non
}else if(isset($_POST['btnNon'])){
    header('location: ./edition.php');    
    exit(); //===> Fin du script
}

// génération de la page
hm_aff_entete('Modifier article', 'Modifier article');
//Si le bouton oui à été cliqué on affiche supprimer article sinon modifier article
if($btnOui==true){
    hml_aff_supprimer_article();
}else{
  hml_aff_modifier_article($erreurs,$bd);  
}


hm_aff_pied();
// fermeture de la connexion à la base de données
mysqli_close($bd);
ob_end_flush(); //FIN DU SCRIPT

/**
 * Affiche les zones de modification d'un article ainsi que 
 * les données de l'article à modifier
 *
 * @param bool      $erreurs  true si erreurs dans les modification de l'article sinon false
 * @param object    $bd       Ouverture base de donnee
 */
function hml_aff_modifier_article($erreurs,$bd){
    $arId=$_SESSION['user']['page'];
    //Requete qui selectionne toutes les infos sur un article
    $sql="SELECT * FROM article WHERE arID='{$arId}'";
    $res=mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    $tab=mysqli_fetch_assoc($res);
    $tab=hm_html_proteger_sortie($tab);
    mysqli_free_result($res);
    $titre=$tab['arTitre'];
    $resume=$tab['arResume'];
    $texte=$tab['arTexte'];
    
    echo '<main>',
        '<section>',
            '<p>Vous pouvez modifier le texte, le titre, le résumé et une éventuelle photo.</p>'; 

    //Si presence d'erreurs on les affiche
    if($erreurs){
        echo '<div class="erreur">Les erreurs suivantes ont été relevées lors de la modification de l\'article<ul>';
        foreach ($erreurs as $err) {
            echo '<li>', $err, '</li>';   
        }
        echo '</ul></div>';
    }
    //Affichage menu edition
    echo '</section></main>';
    
    echo '<main>',
        '<section>',
            '<h2>Informations</h2>', 
            '<form enctype="multipart/form-data" action="edition.php" method="post">';

    echo '<p><fieldset class="cadre_titre"><legend>Ajoutez le titre</legend>',
            '<textarea id="titre" name="titre" rows="1" cols="100">',$titre,'</textarea>',
            '</fieldset></p>',        
        '<p><fieldset class="cadre_resume"><legend>Ajoutez le résumé</legend>',
            '<textarea id="resume" name="resume" rows="5" cols="100">',$resume,'</textarea>',
            '</fieldset></p>',        
        '<p><fieldset class="cadre_texte"><legend>Ajoutez le texte</legend>',
            '<textarea id="texte" name="texte" rows="40" cols="100">',$texte,'</textarea>',
        '</fieldset></p>',
        '<p><fieldset class="cadre_image"><legend>Ajoutez une image</legend>';
        //Si image associé a l'article existe on l'affiche sinon message de non-présence
        if(is_file("../upload/{$arId}.jpg")){
            $imgFile = "../upload/{$arId}.jpg";
            echo '<p><img class="imgRedacteur" src="',$imgFile,'" width="400" height="300"></p>';
        }else{
            echo '<p>Pas d\'image associée à cet article.</p>';
        }
        echo '<input type="file" name="btnChangePhotoArticle">', 
        '</fieldset></p>',
        '<table>',
            '<tr>',
                '<td colspan="2">',
                    '<input type="submit" name="btnModifierArticle" value="Modifier">',
                    '<input type="reset" value="Réinitialiser">', 
                '</td>',
            '</tr>',
        '</table>',   
     '</form>',
    '</section></main>';
    //Affichage section supression
    echo '<main>',
        '<section>',
            '<h2>Suppression</h2>',
            '<form action="edition.php" method="post">';
    echo '<table><td colspan="1"><input type="submit" name="btnSupprimerArticle" value="Supprimer l\'article"></td></table>';
        
        echo '</form>',
        '</section></main>';
}

/**
 * Affiche la demande de confirmation de supression
 * d'un article
 *
 */
function hml_aff_supprimer_article(){
    echo '<main>',
        '<section>',
        '<p>Êtes-vous sûr de vouloir supprimer cet article?</p>',
            '<h2>Confirmation supression</h2>',
            '<form action="edition.php" method="post">';
    echo '<table><td colspan="1"><input type="submit" name="btnOui" value="Oui">';
    echo '<input type="submit" name="btnNon" value="Non"></td></table>';
        
        echo '</form>',
        '</section></main>';
}

/**
 * Vérifie si les donnees de l'article modifié sont valides.
 * Donnees= Titre, Resume, Texte, Photo.
 *
 * @param object    $bd     Ouverture base de donnee
 * 
 * @return string[]         tableau d'erreurs, false si 0 erreur
 */
function hml_traitement_modifier_article($bd){
    $erreurs = array();
    $arId=$_SESSION['user']['page'];
    
    // vérification des titre, résumé et texte
    $arTitre=htmlspecialchars(trim($_POST['titre']));
    $arResume=htmlspecialchars(trim($_POST['resume']));
    $arTexte=htmlspecialchars(trim($_POST['texte']));
    $arTitre=mysqli_real_escape_string($bd,$arTitre);
    $arResume=mysqli_real_escape_string($bd,$arResume);
    $arTexte=mysqli_real_escape_string($bd,$arTexte);
    hm_verifier_texte_article($arTitre, 'Le titre', $erreurs);
    hm_verifier_texte_article($arResume, 'Le résumé', $erreurs);
    hm_verifier_texte_article($arTexte, 'Le texte', $erreurs);

    // vérification de la photo
    $oks=array('.jpg'); //Formats photos acceptés
    if(isset($_FILES['btnChangePhotoArticle']['name'])){
        if($_FILES['btnChangePhotoArticle']['name']!= NULL){
            $nomI=$_FILES['btnChangePhotoArticle']['name'];
            $ext=strtolower(substr($nomI,strrpos($nomI,'.')));
            if(in_array($ext,$oks)){
                $Dest='./../upload/'.strval($arId);
                if($_FILES['btnChangePhotoArticle']['error']==0 && @is_uploaded_file($_FILES['btnChangePhotoArticle']['tmp_name']) && @move_uploaded_file($_FILES['btnChangePhotoArticle']['tmp_name'],$Dest.$ext)){           
                }else{
                    $erreurs[]='Une erreur interne a empêché l\'upload de l\'image.';
                }
            }else{
                $erreurs[]='L\'extension de l\'image doit être .jpg.';
            }
        }
    }

    // si erreurs --> retour
    if (count($erreurs) > 0) {
        return $erreurs;   //===> FIN DE LA FONCTION
    }


    $annee=date('Y');
    $mois=date('m');
    $jour=date('d');
    $heure=date('H');
    $minute=date('i');

    //Requete qui update les infos dans un article
    $sql = "UPDATE article SET arTitre='{$arTitre}', arResume='{$arResume}', arTexte='{$arTexte}',
             arDateModification={$annee}{$mois}{$jour}{$heure}{$minute} 
            WHERE arID='{$arId}'";

    mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    // fermeture de la connexion à la base de données
    mysqli_close($bd);
    
    // redirection sur la page article.php?id=$arId
    $id_crypt=hm_crypteSigneURL($arId);
    header('location: ./article.php?id='.$id_crypt);  
    exit(); //===> Fin du script
}

/**
 * Supprime un article, ces commentaires associés et son image.
 * 
 * Termine le script et redirige vers index.php
 *
 * @param object    $bd     Ouverture base de donnee
 */
function hml_traitement_supprimer_article($bd){
    $arId=$_SESSION['user']['page'];
    $sql="DELETE FROM commentaire WHERE coArticle={$arId}";
    mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    $sql="DELETE FROM article WHERE arID={$arId}";
    mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    
    //Suppression de l'image de l'article du serveur
    if(is_file("../upload/{$arId}.jpg")){
        $imgFile = "../upload/{$arId}.jpg";
        unlink($imgFile);
    }
    // redirection sur la page index.php
    header('location: ./../index.php');    
    exit(); //===> Fin du script

    

    
}