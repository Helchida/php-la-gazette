<?php
require_once('./bibli_gazette.php');
require_once('./bibli_generale.php');

// bufferisation des sorties
ob_start();

// démarrage de la session
session_start();


// si il y a une autre clé que id dans $_GET, piratage ?
// => l'utilisateur est redirigé vers index.php
if (!hm_parametres_controle('get', array(), array('id'))) {
    header('Location: ../index.php');
    exit;
}

// affichage de l'entête
hm_aff_entete('L\'actu', 'L\'actu');

// ouverture de la connexion à la base de données
$bd = hm_bd_connecter();

$id = hm_decrypteSigneURL($_GET['id']);
// vérification du format du paramètre dans l'URL
if (!isset($id)) {
    hml_aff_erreur ('Identifiant d\'article non fourni.');
    return;     // ==> fin de la fonction
}
    
if (!hm_est_entier($id) || $id <= 0) {
    hml_aff_erreur ('Identifiant d\'article invalide.');
    return;     // ==> fin de la fonction
}



// Récupération de l'article, des informations sur son auteur (y compris ses éventuelles infos renseignées dans la table 'redacteur'),
// de ses éventuelles commentaires
$sql = "SELECT *  
        FROM ((article INNER JOIN utilisateur ON arAuteur = utPseudo)
        LEFT OUTER JOIN redacteur ON utPseudo = rePseudo)
        LEFT OUTER JOIN commentaire ON arID = coArticle
        WHERE arID = {$id}
        ORDER BY coDate DESC, coID DESC";
$res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);

$commAff=false;
$dejaAffiche=false;
$err=false;
//Parcours du tableau de l'article et ces infos 
while($tab = mysqli_fetch_assoc($res)){
    $tab=hm_html_proteger_sortie($tab);
    //Nom du bouton par rapport au commentaire à supprimer
    $btnName='btnSupprimer'.strval($tab['coID']);
    //Si l'utilisateur a cliqué sur un bouton de supression de commentaire
    if(isset($_POST[$btnName])){
        $idSuppr=$tab['coID'];
        //Requete qui supprime un commentaire
        $sql="DELETE FROM commentaire WHERE coID={$idSuppr}";
        mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
        // Récupération de l'article, des informations sur son auteur (y compris ses éventuelles infos renseignées dans la table 'redacteur'),
        // de ses éventuelles commentaires
        $sql = "SELECT *  
        FROM ((article INNER JOIN utilisateur ON arAuteur = utPseudo)
        LEFT OUTER JOIN redacteur ON utPseudo = rePseudo)
        LEFT OUTER JOIN commentaire ON arID = coArticle
        WHERE arID = {$id}
        ORDER BY coDate DESC, coID DESC";
        
    }
    //Si l'utilisateur a cliqué sur le bouton publier commentaire
    if(isset($_POST['btnEnvoyerComm']) && $commAff==false){
        $err=hml_inserer_comm($bd,$id);
        $commAff=true;
        // Récupération de l'article, des informations sur son auteur (y compris ses éventuelles infos renseignées dans la table 'redacteur'),
        // de ses éventuelles commentaires
        $sql = "SELECT *  
        FROM ((article INNER JOIN utilisateur ON arAuteur = utPseudo)
        LEFT OUTER JOIN redacteur ON utPseudo = rePseudo)
        LEFT OUTER JOIN commentaire ON arID = coArticle
        WHERE arID = {$id}
        ORDER BY coDate DESC, coID DESC";
        

    }
    //Si l'utilisateur est connecté
    if(isset($_SESSION['user']['pseudo'])){
        //Si l'utilisateur est le redacteur de l'article et a le statut redacteur
        if($_SESSION['user']['pseudo']==$tab['arAuteur'] && $dejaAffiche==false && (($tab['utStatut']==1)||($tab['utStatut']==3))){
            echo  '<main><section>',
            '<p>Vous êtes l\'auteur de cet article, <a href="edition.php">cliquez ici pour le modifier ou le supprimer</a>.</p>';
            echo '</main>';
            $dejaAffiche=true;
            $_SESSION['user']['page']=$id;
        }
        
    }
}
$res = mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
echo '<main>';
// affichage du contenu (article + commentaires)
hml_aff_article($bd,$res,$id,$err);
echo '</main>';
// pied de page
hm_aff_pied();

// fin du script
ob_end_flush();


/**
 * Affiche l'article ainsi que ces commentaires, une zone
 * pour écrire un commentaire si utilisateur authentifié,
 * une section pour modifier article si redacteur de l'article.
 *
 * @param object    $bd        Ouverture base de donnee
 * @param bool      $res       Resultat requete 
 * @param int       $id        id de l'article en cours de visionnage
 * @param bool      $err       erreur a propos du commentaire, true si texte vide sinon false
 */
function hml_aff_article($bd,$res,$id,$err) {
    // pas d'articles --> fin de la fonction
    if (mysqli_num_rows($res) == 0) {
        hml_aff_erreur ('Identifiant d\'article non reconnu.');
        mysqli_free_result($res);
        mysqli_close($bd);
        return;         // ==> fin de la fonction
    }
    // ---------------- GENERATION DE L'ARTICLE ------------------
    // affichage de l'article et des commentaires associés
    $tab = mysqli_fetch_assoc($res);
    // Mise en forme du prénom et du nom de l'auteur pour affichage dans le pied du texte de l'article
    // Par exemple, pour 'johnny' 'bigOUde', ça donne 'J. Bigoude'
    // A faire avant la protection avec htmlentities() à cause des éventuels accents
    $auteur = hml_mb_ucfirst_lcremainder(mb_substr($tab['utPrenom'], 0, 1, 'UTF-8')) . '. ' . hml_mb_ucfirst_lcremainder($tab['utNom']);
    // protection contre les attaques XSS
    $auteur = hm_html_proteger_sortie($auteur);
    // protection contre les attaques XSS
    $tab = hm_html_proteger_sortie($tab);
    $imgFile = "../upload/{$id}.jpg";
    // génération du bloc <article>
    echo '<article>', 
            '<h3>', $tab['arTitre'], '</h3>',
            ((file_exists($imgFile)) ? "<img src='{$imgFile}' alt=\"Photo d\'illustration | {$tab['arTitre']}\">" : ''),
            hm_BBCodeToHTML($tab['arTexte']),        
            '<footer>Par ',
            // si l'auteur a encore le droit de rédacteur et si il a enregistré des informations dans la table redacteur
            // on affiche un lien vers sa présentation sur la page redaction.php, 
            // sinon on affiche uniquement $auteur
            ((isset($tab['rePseudo']) && ($tab['utStatut'] == 1 || $tab['utStatut'] == 3)) ?
            "<a href='../php/redaction.php#{$tab['utPseudo']}'>$auteur</a>" : $auteur), 
            '. Publié le ', hml_date_to_string($tab['arDatePublication']);

    // ajout dans le pied d'article d'une éventuelle date de modification
    if (isset($tab['arDateModification'])) {
        echo ', modifié le '. hml_date_to_string($tab['arDateModification']);
    }
    
    // fin du bloc <article>
    echo '</footer>',                
        '</article>';

    //pour accéder une seconde fois au premier enregistrement de la sélection
    mysqli_data_seek($res, 0); 

    // Génération du début de la zone de commentaires
    echo '<section>',
            '<h2>Réactions</h2>';    
    if ($err) {
        echo '<div class="erreur">Le commentaire ne doit pas être vide</div>';
    }else if(isset($_POST['btnEnvoyerComm'])){
        echo '<div class="reussite">Le commentaire est ajouté.</div>';
    }
    // s'il existe des commentaires, on les affiche un par un.
    if (isset($tab['coID'])) {
        echo '<ul>';
        while ($tab = mysqli_fetch_assoc($res)) {
            $tab=hm_html_proteger_sortie($tab);
            $id_crypt=hm_crypteSigneURL($tab['arID']);
            echo '<li>',
            '<form action="article.php?id=',$id_crypt,'" method="post">',
                    '<p>Commentaire de <strong>', $tab['coAuteur'], '</strong>, le ',
                        hml_date_to_string($tab['coDate']); 
                    if(isset($_SESSION['user'])){
                        $name='btnSupprimer'.strval($tab['coID']);
                        if(($tab['coAuteur']==$_SESSION['user']['pseudo']) || ($_SESSION['user']['redacteur'])){
                            echo ' <input type="submit" name="',$name,'" value="Supprimer le commentaire">';
                        }
                    }
                    
                    echo '</p></form>';
                    echo '<blockquote>',
                    hm_BBCodeToHTML($tab['coTexte']),
                    '</blockquote>', 
                '</li>';
        }
        echo '</ul>';
    }
    // sinon on indique qu'il n'y a pas de commentaires
    else {
        echo '<p>Il n\'y a pas de commentaires à cet article. </p>';  
    }
    echo '<p>'; 
    //Si l'utilisateur n'est pas connecté sinon il peut ajouter un commentaire
    if(!isset($_SESSION['user'])){
       echo '<a href="connexion.php">Connectez-vous</a> ou <a href="inscription.php">inscrivez-vous</a> ',
                'pour pouvoir commenter cet article !';
    }else{
            $id_crypt=hm_crypteSigneURL($id);
            echo '<form action="article.php?id=',$id_crypt,'" method="post">';
        
            echo '<fieldset class="cadre_comm"><legend>Ajoutez un commentaire</legend>';
                echo '<textarea id="comm" name="comm" rows="20" cols="100"></textarea>';
                echo ' <p><input type="submit" name="btnEnvoyerComm" value="Publier ce commentaire"></p>';
            echo '</fieldset>';
        echo '</form>';
       
    }
                
    echo '</p>',  
        '</section>';

    // libération des ressources
    mysqli_free_result($res);
    // fermeture de la connexion à la base de données
    mysqli_close($bd);
}

/**
 * Insere un commentaire dans la base de donnée
 *
 * @param object    $bd       Ouverture base de donnee
 * @param int       $id       id de l'article en cours de visionnage
 * 
 * @return bool     false si pas d'erreur dans le commentaire, true si vide.
 */
function hml_inserer_comm($bd,$id){
    //Requete qui selectionne le nombre de commentaire
    $sql="SELECT COUNT(coID) as Nb_comm FROM commentaire";
    $res2=mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    $tab=mysqli_fetch_assoc($res2);
    $tab=hm_html_proteger_sortie($tab);
    $coId=$tab['Nb_comm'];
    mysqli_free_result($res2);
    //Requete qui selectionne les id des commentaires
    $sql="SELECT coID FROM commentaire";
    $res2=mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    $coAuteur=$_SESSION['user']['pseudo'];
    $coTexte=htmlspecialchars($_POST['comm']);
    $coTexte=mysqli_real_escape_string($bd,$coTexte);
    //Si texte commentaire est vide return true (donc une erreur)
    if(empty($coTexte)){
        return true;
    }
    $annee=date('Y');
    $mois=date('m');
    $jour=date('d');
    $heure=date('H');
    $minute=date('i');
    //Parcours du tableau des id de commentaire
    while($tab3=mysqli_fetch_assoc($res2)){
        $tab3=hm_html_proteger_sortie($tab3);
        //Si id du commentaire est deja egal a un du tableau on augmente l'id de 1
        if($coId==$tab3['coID']){
            $coId++;
        }
    }
    mysqli_free_result($res2);
    //Requete qui insert un commentaire
    $sql="INSERT INTO commentaire(coID, coAuteur, coTexte, coDate, coArticle) 
        VALUES ('{$coId}','{$coAuteur}','{$coTexte}', '{$annee}{$mois}{$jour}{$heure}{$minute}', '{$id}')";
    mysqli_query($bd, $sql) or hm_bd_erreur($bd, $sql);
    //Retourne faux si il n'y a pas d'erreur
    return false;
}


//_______________________________________________________________
/**
 *  Conversion d'une date format AAAAMMJJHHMM au format JJ mois AAAA à HHhMM
 *
 *  @param  int     $date   la date à afficher. 
 *  @return string          la chaîne qui reprsente la date
 */
function hml_date_to_string($date) {
    // les champs date (coDate, arDatePublication, arDateModification) sont de type BIGINT dans la base de données
    // donc pas besoin de les protéger avec htmlentities()
    
    // si un article a été publié avant l'an 1000, ça marche encore :-)
    $min = substr($date, -2);
    $heure = (int)substr($date, -4, 2); //conversion en int pour supprimer le 0 de '07' pax exemple
    $jour = (int)substr($date, -6, 2);
    $mois = substr($date, -8, 2);
    $annee = substr($date, 0, -8);
    
    $month = hm_get_tableau_mois();    
    
    return $jour. ' '. mb_strtolower($month[$mois - 1], 'UTF-8'). ' '. $annee . ' à ' . $heure . 'h' . $min;
    // mb_* -> pour l'UTF-8, voir : https://www.php.net/manual/fr/function.mb-strtolower.php
}

//___________________________________________________________________
/**
 * Renvoie une copie de la chaîne UTF8 transmise en paramètre après avoir mis sa
 * première lettre en majuscule et toutes les suivantes en minuscule
 *
 * @param  string   $str    la chaîne à transformer
 * @return string           la chaîne résultat
 */
function hml_mb_ucfirst_lcremainder($str) {
    $str = mb_strtolower($str, 'UTF-8');
    $fc = mb_strtoupper(mb_substr($str, 0, 1, 'UTF-8'));
    return $fc.mb_substr($str, 1, mb_strlen($str), 'UTF-8');
}

//_______________________________________________________________
/**
 *  Affchage d'un message d'erreur dans une zone dédiée de la page.
 *  @param  String  $msg    le message d'erreur à afficher.
 */
function hml_aff_erreur($msg) {
    echo '<main>', 
            '<section>', 
                '<h2>Oups, il y a une erreur...</h2>',
                '<p>La page que vous avez demandée a terminé son exécution avec le message d\'erreur suivant :</p>',
                '<blockquote>', $msg, '</blockquote>', 
            '</section>', 
        '</main>';
}



?>
